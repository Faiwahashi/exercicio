namespace Detetive.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Jogo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Jogos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Arma_ArmaTipo = c.Int(nullable: false),
                        Local_LocalTipo = c.Int(nullable: false),
                        Suspeito_SuspeitoTipo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Jogos");
        }
    }
}
