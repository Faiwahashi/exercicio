﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using Detetive.Models;
using System;
using System.Net.Http;
using System.Collections.Generic;

namespace Detetive.Controllers
{
    public class DetetiveController : ApiController
    {
        
        private JogoDb jogoDb = new JogoDb();

        [Route("api/getarmas")]
        public IQueryable<ArmaTipo> GetArmas()
        {
            return Enum.GetValues(typeof(ArmaTipoEnum))
                    .Cast<ArmaTipoEnum>()
                    .Select(v => new  ArmaTipo
                    {
                        Id = ((int)v),
                        Nome = v.ToString()
                    })
                    .ToList().AsQueryable();
        }

        [Route("api/getsuspeitos")]
        public IQueryable<SuspeitoTipo> GetSuspeitos()
        {
            return Enum.GetValues(typeof(SuspeitoTipoEnum))
                    .Cast<SuspeitoTipoEnum>()
                    .Select(v => new SuspeitoTipo
                    {
                        Id = ((int)v),
                        Nome = v.ToString()
                    })
                    .ToList().AsQueryable();
        }

        [Route("api/getlocais")]
        public IQueryable<LocalTipo> GetLocais()
        {
            return Enum.GetValues(typeof(LocalTipoEnum))
                    .Cast<LocalTipoEnum>()
                    .Select(v => new LocalTipo
                    {
                        Id = ((int)v),
                        Nome = v.ToString()
                    })
                    .ToList().AsQueryable();
        }

        [HttpPost, Route("api/verificar")]
        public HttpResponseMessage Verificar(Jogo jogo)
        {
            Jogo jogoDb = this.jogoDb.Jogos.Find(jogo.Id);

            string messageError = null;

            if (!(jogoDb.Arma.ArmaTipo == (ArmaTipoEnum)jogo.Arma.ArmaTipo))
            {
                messageError = "Arma incorreta";
            }

            if (!(jogoDb.Local.LocalTipo == (LocalTipoEnum)jogo.Local.LocalTipo))
            {
                messageError = "Local incorreto";
            }

            if (!(jogoDb.Suspeito.SuspeitoTipo == (SuspeitoTipoEnum)jogo.Suspeito.SuspeitoTipo))
            {
                
                messageError = ("Suspeito incorreto");
            }

            if (messageError == null)
                return Request.CreateResponse(HttpStatusCode.OK,"Parabéns você solucionou o caso!");
            else
                return Request.CreateResponse(HttpStatusCode.NotFound, messageError);            
        }

        [ResponseType(typeof(Jogo))]
        [Route("api/sortear")]
        public int Sortear()
        {
            Jogo jogo = new Jogo();
            var rand = new Random();

            ArmaTipoEnum[] ArmaAllValues = (ArmaTipoEnum[])Enum.GetValues(typeof(ArmaTipoEnum));
            var armaValue = ArmaAllValues[rand.Next(ArmaAllValues.Length)];
            jogo.Arma = new Arma
            {
                ArmaTipo = armaValue
            };

            LocalTipoEnum[] LocalAllValues = (LocalTipoEnum[])Enum.GetValues(typeof(LocalTipoEnum));
            var localValue = LocalAllValues[rand.Next(LocalAllValues.Length)];
            jogo.Local = new Local
            {
                LocalTipo = localValue
            };

            SuspeitoTipoEnum[] SuspeitoAllValues = (SuspeitoTipoEnum[])Enum.GetValues(typeof(SuspeitoTipoEnum));
            var suspeitoValue = SuspeitoAllValues[rand.Next(SuspeitoAllValues.Length)];
            jogo.Suspeito = new Suspeito
            {
                SuspeitoTipo = suspeitoValue
            };

            jogoDb.Jogos.Add(jogo);
            jogoDb.SaveChanges();

            var response = Request.CreateResponse(HttpStatusCode.Created);

            return jogo.Id;
        }
    }
}