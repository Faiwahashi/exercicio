﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Detetive.Models
{
    public class JogoDb : DbContext
    {
        public DbSet<Jogo> Jogos { get; set; }
    }
}