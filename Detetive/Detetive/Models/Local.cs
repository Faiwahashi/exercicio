﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Detetive.Models
{
    public class Local
    {
        public LocalTipoEnum LocalTipo { get; set; }
    }

    public class LocalTipo
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }

    public enum LocalTipoEnum
    {
        Eternia = 1,
        Vulcano = 2,
        Tatooine = 3,
        Springfield = 4,
        Gotham = 5,
        NovaYork = 6,
        Siberia = 7,
        MachuPichu = 8,
        ShowDoKatinguele = 9,
        SaoPaulo = 10
    }
}