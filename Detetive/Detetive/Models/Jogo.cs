﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Detetive.Models
{
    [Table("Jogos")]
    public class Jogo
    {
        [Key]
        public int Id { get; set; }
        public Arma Arma { get; set; }
        public Local Local { get; set; }
        public Suspeito Suspeito { get; set; }
    }
}