﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Detetive.Models
{
    public class Suspeito
    {
        public SuspeitoTipoEnum SuspeitoTipo { get; set; }
    }

    public class SuspeitoTipo
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }

    public enum SuspeitoTipoEnum
    {
        Esqueleto = 1,
        Khan = 2,
        DarthVader = 3,
        SideShowBob = 4,
        Coringa = 5,
        DuendeVerde = 6
    }
}