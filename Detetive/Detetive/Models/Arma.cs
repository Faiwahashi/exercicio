﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Detetive.Models
{
    public class Arma
    {
        public ArmaTipoEnum ArmaTipo { get; set; }
    }

    public class ArmaTipo
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }

    public enum ArmaTipoEnum
    {
        CajadoDevastador = 1,
        Phaser = 2,
        Peixeira = 3,
        Trezoitao = 4,
        SabreDeLuz = 5,
        Bomba = 6
    }
}