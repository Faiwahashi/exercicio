﻿(function (app) {

var jogaController = function ($scope, detetiveService) {
    
    $scope.isEditavel = function () {
        return $scope.editar && $scope.editar.jogo;
    };

    $scope.reinicar = function () {
        $scope.returnmessage = null;
        $scope.acertou = null;
        $scope.showSortear = true;
    };

    $scope.verificar = function () {
        detetiveService
            .verificar($scope.jogoId, $scope.arma, $scope.local, $scope.suspeito)
            .success(function(data){
                $scope.returnmessage = data;
                $scope.acertou = true;
            })
            .error(function (data) {
                $scope.returnmessage = data;
                $scope.acertou = false;
            });
    };
  };

app.controller("jogaController", jogaController);

}(angular.module("detetive")));