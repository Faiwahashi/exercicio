﻿(function (app) {

    var iniciaController = function ($scope, detetiveService)
    {

        $scope.criar = function () {          

            $scope.hideSortear = true;

            $scope.editar = {
                jogo: {
                    value: true
                }
            };

            detetiveService
            .getArmas()
            .success(function (data) {
                $scope.armas = data;
            });

            detetiveService
            .getLocais()
            .success(function (data) {
                $scope.locais = data;
            });

            detetiveService
            .getSuspeitos()
            .success(function (data) {
                $scope.suspeitos = data;
            });

            detetiveService
            .sortear()
            .success(function (data) {
                $scope.jogoId = data;
            });
        };


        $scope.sortear = function () {
            detetiveService.sortear()
        };

    };

    app.controller("iniciaController", iniciaController)

}(angular.module("detetive")));