﻿(function (app) {
     
    var detetiveService = function ($http) {        

        var getArmas = function(){
            return $http({
                method: 'GET',
                url: 'api/getarmas'
            })
        };

        var getLocais = function () {
            return $http({
                method: 'GET',
                url: 'api/getlocais'
            })
        };

        var getSuspeitos = function () {
            return $http({
                method: 'GET',
                url: 'api/getsuspeitos'
            })
        };

        var sortear = function () {
            return $http({
                method: 'POST',
                url: 'api/sortear'
                })
        };

        var verificar = function (id, arma, local, suspeito) {
            var jogo = new Object();

            jogo.Id = id;
            jogo.Arma = new Object();
            jogo.Arma.ArmaTipo = arma;
            jogo.Local = new Object();
            jogo.Local.LocalTipo = local;
            jogo.Suspeito = new Object();
            jogo.Suspeito.SuspeitoTipo = suspeito;

            return $http({
                method: 'POST',
                url: 'api/verificar',
                data:jogo,
                contentType: 'application/json'
            })  
        };


        return {
            sortear: sortear,
            getArmas: getArmas,
            getLocais: getLocais,
            getSuspeitos: getSuspeitos,
            verificar: verificar
        };
    };

    app.factory("detetiveService", detetiveService);

}(angular.module("detetive")))