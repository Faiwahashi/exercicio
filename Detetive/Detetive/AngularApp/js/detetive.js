﻿(function () {

    var app = angular.module("detetive", ["ngRoute"]);

    var config = function ($routeProvider) {

        $routeProvider
        .when("/",
               { templateUrl: "/cliente/html/inicia.html"})
        .otherwise(
               { redirecTo: "/"});
    };
    app.config(config);    
}());
